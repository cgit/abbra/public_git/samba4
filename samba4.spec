%define main_release 108

%define samba_version 4.0.0
%define talloc_version 2.0.7
%define tdb_version 1.2.9
%define tevent_version 0.9.15
%define ldb_version 1.1.4
%define pre_release alpha18

%define samba_release %{main_release}%{pre_release}%{?dist}

%define with_pam_smbpass 0
%define with_talloc 1
%define with_tevent 1
%define with_tdb 1
%define with_ldb 1

%define with_usrmove 0

%if 0%{?fedora} > 15 || 0%{?rhel} > 6
%define with_talloc 0
%define with_tevent 0
%define with_tdb 0
%define with_ldb 0
%endif

%if 0%{?fedora} > 16
%define with_usrmove 1
%endif

%if %with_usrmove
%define smb_lib %{_libdir}
%else
%define smb_lib %{_lib}
%endif

Name:           samba4
Version:        %{samba_version}
Release:        %{samba_release}
Epoch:          1

%if 0%{?epoch} > 0
%define samba_depver %{epoch}:%{version}-%{release}
%else
%define samba_depver %{version}-%{release}
%endif

Summary:        Server and Client software to interoperate with Windows machines
License:        GPLv3+ and LGPLv3+
Group:          System Environment/Daemons
URL:            http://www.samba.org/

Source0:        samba-%{version}%{pre_release}.tar.bz2

# Red Hat specific replacement-files
Source1: samba.log
Source2: samba.xinetd
Source3: swat.desktop
Source4: smb.conf.default
Source5: pam_winbind.conf
Source6: samba.pamd
Source7: samba.conf.tmp

Source100: nmb.init
Source101: smb.init
Source102: winbind.init

Patch1: samba4-libpdb-soversion.patch
Patch2: samba4-samr-lsa-session_key.patch
Patch3: samba4-waf_target.patch
Patch4: samba4-external_ldap_callback.patch

BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

Requires(pre): /usr/sbin/groupadd
%if 0%{?fedora} > 15 || 0%{?rhel} > 6
Requires(post): /bin/systemctl
Requires(preun): /bin/systemctl
%else
Requires(post): /sbin/chkconfig, /sbin/service
Requires(preun): /sbin/chkconfig, /sbin/service
%endif

Requires(pre): %{name}-common = %{samba_depver}
Requires: logrotate
Requires: pam
Requires: perl(Parse::Yapp)

Conflicts: samba < %{samba_depver}
Provides: samba = %{samba_depver}

BuildRequires: autoconf
BuildRequires: ctdb-devel
BuildRequires: cups-devel
BuildRequires: docbook-style-xsl
BuildRequires: e2fsprogs-devel
BuildRequires: gawk
BuildRequires: krb5-devel
BuildRequires: libacl-devel
BuildRequires: libaio-devel
BuildRequires: libattr-devel
BuildRequires: libcap-devel
BuildRequires: libuuid-devel
BuildRequires: libxslt
BuildRequires: ncurses-devel
BuildRequires: openldap-devel
BuildRequires: pam-devel
BuildRequires: perl(ExtUtils::MakeMaker)
BuildRequires: perl(Parse::Yapp)
BuildRequires: popt-devel
BuildRequires: python-devel
BuildRequires: quota-devel
BuildRequires: readline-devel
BuildRequires: sed
BuildRequires: zlib-devel >= 1.2.3
BuildRequires: libbsd-devel

%if ! %with_talloc
%define libtalloc_version 2.0.6

BuildRequires: libtalloc-devel >= %{libtalloc_version}
BuildRequires: pytalloc-devel >= %{libtalloc_version}
%endif

%if ! %with_tevent
%define libtevent_version 0.9.13

BuildRequires: libtevent-devel >= %{libtevent_version}
%endif

%if ! %with_ldb
%define libldb_version 1.1.0

BuildRequires: libldb-devel >= %{libldb_version}
BuildRequires: pyldb-devel >= %{libldb_version}
%endif

%if ! %with_tdb
%define libtdb_version 1.2.9

BuildRequires: libtdb-devel >= %{libtdb_version}
%endif

%description
Samba is the standard Windows interoperability suite of programs for Linux and Unix.

%package client
Summary: Samba client programs
Group: Applications/System
Requires: %{name}-common = %{samba_depver}
Requires: %{name}-libs = %{samba_depver}

Conflicts: samba-client < 1:3.9.9
Provides: samba-client = %{samba_depver}

%description client
The samba4-client package provides some SMB/CIFS clients to complement
the built-in SMB/CIFS filesystem in Linux. These clients allow access
of SMB/CIFS shares and printing to SMB/CIFS printers.

%package libs
Summary: Samba libraries
Group: Applications/System
Requires: libwbclient

%description libs
The samba4-libs package contains the libraries needed by programs that
link against the SMB, RPC and other protocols provided by the Samba suite.

%package python
Summary: Samba Python libraries
Group: Applications/System
Requires: %{name}-libs = %{samba_depver}

%description python
The samba4-python package contains the Python libraries needed by programs
that use SMB, RPC and other Samba provided protocols in Python programs.

%package devel
Summary: Developer tools for Samba libraries
Group: Development/Libraries
Requires: %{name}-libs = %{samba_depver}

Conflicts: samba-devel < 1:3.9.9
Provides: samba-devel = %{samba_depver}

%description devel
The samba4-devel package contains the header files for the libraries
needed to develop programs that link against the SMB, RPC and other
libraries in the Samba suite.

%package pidl
Summary: Perl IDL compiler
Group: Development/Tools
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description pidl
The samba4-pidl package contains the Perl IDL compiler used by Samba
and Wireshark to parse IDL and similar protocols

%package common
Summary: Files used by both Samba servers and clients
Group: Applications/System
Requires: %{name}-libs = %{samba_depver}

Conflicts: samba-common < 1:3.9.9
Provides: samba-common = %{samba_depver}

%description common
samba4-common provides files necessary for both the server and client
packages of Samba.

%package test
Summary: Testing tools for Samba servers and clients
Group: Applications/System

%description test
samba4-test provides testing tools for both the server and client
packages of Samba.

%package winbind
Summary: Samba winbind
Group: Applications/System
Requires: %{name} = %{samba_depver}

Conflicts: samba-winbind < 1:3.9.9
Provides: samba-winbind = %{samba_depver}

%description winbind
The samba-winbind package provides the winbind NSS library, and some
client tools.  Winbind enables Linux to be a full member in Windows
domains and to use Windows user and group accounts on Linux.

%package winbind-krb5-locator
Summary: Samba winbind krb5 locator
Requires: %{name}-winbind-clients = %{samba_depver}
Group: Applications/System

Conflicts: samba-winbind-krb5-locator < %{samba_depver}
Provides: samba-winbind-krb5-locator = %{samba_depver}

%description winbind-krb5-locator
The winbind krb5 locator is a plugin for the system kerberos library to allow
the local kerberos library to use the same KDC as samba and winbind use

%package winbind-clients
Summary: Samba winbind clients
Group: Applications/System
Requires: libwbclient

Conflicts: samba-winbind-clients < %{samba_depver}

%description winbind-clients
The samba-winbind-clients package provides the NSS library and a PAM
module necessary to communicate to the Winbind Daemon


%package swat
Summary: The Samba SMB server Web configuration program
Group: Applications/System
Requires: %{name} = %{samba_depver}, xinetd

%description swat
The samba-swat package includes the new SWAT (Samba Web Administration
Tool), for remotely managing Samba's smb.conf file using your favorite
Web browser.

%package -n libsmbclient4
Summary: The SMB client library
Group: Applications/System
Requires: %{name}-common = %{samba_depver}

Conflicts: libsmbclient < 1:3.9.9
Provides: libsmbclient = %{samba_depver}

%description -n libsmbclient4
The libsmbclient4 contains the SMB client library from the Samba suite.

%package -n libsmbclient4-devel
Summary: Developer tools for the SMB client library
Group: Development/Libraries
Requires: libsmbclient4 = %{samba_depver}

Conflicts: libsmbclient-devel < 1:3.9.9
Provides: libsmbclient-devel = %{samba_depver}

%description -n libsmbclient4-devel
The libsmbclient4-devel package contains the header files and libraries needed to
develop programs that link against the SMB client library in the Samba suite.

%package -n libwbclient
Summary: The winbind client library
Group: Applications/System

%description -n libwbclient
The libwbclient package contains the winbind client library from the Samba suite.

%package -n libwbclient-devel
Summary: Developer tools for the winbind library
Group: Development/Libraries
Requires: libwbclient = %{samba_depver}

%description -n libwbclient-devel
The libwbclient-devel package provides developer tools for the wbclient library.

%package dc
Summary: Samba AD Domain Controller
Group: Applications/System
Requires: samba4-dc-libs = %{samba_depver}

%description dc
The samba-dc package provides AD Domain Controller functionality

%package dc-libs
Summary: Samba AD Domain Controller Libraries
Group: Applications/System
Requires: samba4-libs = %{samba_depver}
Requires: samba4-common = %{samba_depver}

%description dc-libs
The samba4-dc-libs package contains the libraries needed by the DC to
link against the SMB, RPC and other protocols.

%prep
%setup -q -n samba-%{version}%{pre_release}

%patch1 -p1 -b .waf_tdb
%patch2 -p1 -b .session_key
%patch3 -p1 -b .waf_target
%patch4 -p1 -b .external_callback

%build
%define _talloc_lib %nil
%define _tevent_lib %nil
%define _tdb_lib %nil
%define _ldb_lib %nil

%if ! %with_talloc
%define _talloc_lib ,!talloc
%endif

%if ! %with_tevent
%define _tevent_lib ,!tevent
%endif

%if ! %with_tdb
%define _tdb_lib ,!tdb
%endif

%if ! %with_ldb
%define _ldb_lib ,!ldb
%endif

%define _samba4_libraries heimdal,!zlib,!popt%{_talloc_lib}%{_tevent_lib}%{_tdb_lib}%{_ldb_lib}

%define _samba4_idmap_modules idmap_ad,idmap_rid,idmap_adex,idmap_hash,idmap_tdb2
%define _samba4_pdb_modules pdb_tdbsam,pdb_ldap,pdb_ads,pdb_smbpasswd,pdb_wbc_sam,pdb_samba4
%define _samba4_auth_modules auth_sam,auth_unix,auth_winbind,auth_wbc,auth_server,auth_builtin,auth_netlogond,auth_script,auth_samba4
# auth_domain needs to be static

%define _samba4_modules %{_samba4_idmap_modules},%{_samba4_pdb_modules},%{_samba4_auth_modules}

%configure \
        --enable-fhs \
        --with-piddir=/run \
        --with-sockets-dir=/run/samba \
        --with-modulesdir=%{_libdir}/samba \
        --with-pammodulesdir=/%{smb_lib}/security \
        --with-lockdir=/var/lib/samba \
        --disable-tdb2 \
        --disable-gnutls \
        --disable-rpath-install \
        --with-shared-modules=%{_samba4_modules} \
        --builtin-libraries=ccan \
        --bundled-libraries=%{_samba4_libraries} \
%if ! %with_pam_smbpass
        --without-pam_smbpass
%endif

export WAFCACHE=/tmp/wafcache
mkdir -p $WAFCACHE
make %{?_smp_mflags}

# Build PIDL for installation into vendor directories before
# 'make proto' gets to it.
(cd pidl && %{__perl} Makefile.PL INSTALLDIRS=vendor )

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

install -d -m 0755 %{buildroot}/usr/{sbin,bin}
install -d -m 0755 %{buildroot}/%{_sysconfdir}/{pam.d,logrotate.d,security}
install -d -m 0755 %{buildroot}/%{smb_lib}/security
install -d -m 0755 %{buildroot}/var/lib/samba
install -d -m 0755 %{buildroot}/var/lib/samba/private
install -d -m 0755 %{buildroot}/var/lib/samba/winbindd_privileged
install -d -m 0755 %{buildroot}/var/lib/samba/scripts
install -d -m 0755 %{buildroot}/var/lib/samba/sysvol
install -d -m 0755 %{buildroot}/var/log/samba/old
install -d -m 0755 %{buildroot}/var/spool/samba
install -d -m 0755 %{buildroot}/%{_datadir}/swat/using_samba
install -d -m 0755 %{buildroot}/var/run/winbindd
install -d -m 0755 %{buildroot}/%{_libdir}/samba
install -d -m 0755 %{buildroot}/%{_libdir}/pkgconfig

# Undo the PIDL install, we want to try again with the right options.
rm -rf %{buildroot}/%{_libdir}/perl5
rm -rf %{buildroot}/%{_datadir}/perl5

# Install PIDL.
( cd pidl && make install PERL_INSTALL_ROOT=%{buildroot} )

# winbind
%if ! %with_usrmove
install -d -m 0755 %{buildroot}%{_libdir}
install -d -m 0755 %{buildroot}/%{smb_lib}
mv -f %{buildroot}/%{_libdir}/libnss_winbind.so.2 %{buildroot}/%{smb_lib}/libnss_winbind.so.2
chmod 0755 %{buildroot}/%{smb_lib}/libnss_winbind.so.2
mv -f %{buildroot}/%{_libdir}/libnss_wins.so.2 %{buildroot}/%{smb_lib}/libnss_wins.so.2
chmod 0755 %{buildroot}/%{smb_lib}/libnss_wins.so.2
%endif
ln -sf /%{smb_lib}/libnss_winbind.so.2  %{buildroot}%{_libdir}/libnss_winbind.so
ln -sf /%{smb_lib}/libnss_wins.so.2  %{buildroot}%{_libdir}/libnss_wins.so

# pdb FIXME
ln -sf %{_libdir}/samba/libpdb.so.0  %{buildroot}%{_libdir}/samba/libpdb.so

# Install other stuff
install -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/logrotate.d/samba
install -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/samba/smb.conf
install -m 0644 %{SOURCE5} %{buildroot}%{_sysconfdir}/security/pam_winbind.conf
install -m 0644 %{SOURCE6} %{buildroot}%{_sysconfdir}/pam.d/samba

echo 127.0.0.1 localhost > %{buildroot}%{_sysconfdir}/samba/lmhosts

install -d -m 0755 %{buildroot}%{_sysconfdir}/openldap/schema
install -m644 examples/LDAP/samba.schema %{buildroot}%{_sysconfdir}/openldap/schema/samba.schema

install -d -m 0755 %{buildroot}%{_sysconfdir}/xinetd.d
install -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/xinetd.d/swat

install -d -m 0755 %{buildroot}%{_sysconfdir}/tmpfiles.d/
install -m644 %{SOURCE7} %{buildroot}%{_sysconfdir}/tmpfiles.d/samba.conf

install -d -m 0755 %{buildroot}%{_sysconfdir}/sysconfig
install -m 0644 packaging/systemd/samba.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/samba

%if 0%{?fedora} > 15 || 0%{?rhel} > 6
install -d -m 0755 %{buildroot}%{_unitdir}
install -m 0644 packaging/systemd/nmb.service %{buildroot}%{_unitdir}/nmb.service
install -m 0644 packaging/systemd/smb.service %{buildroot}%{_unitdir}/smb.service
install -m 0644 packaging/systemd/winbind.service %{buildroot}%{_unitdir}/winbind.service
%endif

%if 0%{?rhel} == 6
install -d -m 0755 %{buildroot}%{_initrddir}
install -m 0644 %{SOURCE100} %{buildroot}%{_initrddir}/nmb
install -m 0644 %{SOURCE101} %{buildroot}%{_initrddir}/smb
install -m 0644 %{SOURCE102} %{buildroot}%{_initrddir}/winbind
%endif

# winbind krb5 locator
install -d -m 0755 %{buildroot}%{_libdir}/krb5/plugins/libkrb5
install -m 755 %{buildroot}/%{_libdir}/winbind_krb5_locator.so %{buildroot}/%{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so
rm -f %{buildroot}/%{_libdir}/winbind_krb5_locator.so

# cleanup stuff that does not belong here
rm -f %{buildroot}/%{_mandir}/man3/ldb.3*
rm -f %{buildroot}/%{_mandir}/man3/talloc.3*

# Clean out crap left behind by the PIDL install.
find %{buildroot} -type f -name .packlist -exec rm -f {} \;
rm -f %{buildroot}%{perl_vendorlib}/wscript_build
rm -rf %{buildroot}%{perl_vendorlib}/Parse/Yapp

# This makes the right links, as rpmlint requires that
# the ldconfig-created links be recorded in the RPM.
/sbin/ldconfig -N -n %{buildroot}%{_libdir}

# Fix up permission on perl install.
%{_fixperms} %{buildroot}%{perl_vendorlib}

# Remove stuff the buildsystem did not handle correctly
rm -f %{buildroot}/%{smb_lib}/security/pam_smbpass.so
rm -f %{buildroot}%{python_sitelib}/tevent.py

%post
%if 0%{?rhel} == 6
/sbin/chkconfig --add smb
/sbin/chkconfig --add nmb
if [ "$1" -ge "1" ]; then
    /sbin/service smb condrestart >/dev/null 2>&1 || :
    /sbin/service nmb condrestart >/dev/null 2>&1 || :
fi
exit 0
%endif

if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
%if 0%{?rhel} == 6
if [ $1 = 0 ] ; then
    /sbin/service smb stop >/dev/null 2>&1 || :
    /sbin/service nmb stop >/dev/null 2>&1 || :
    /sbin/chkconfig --del smb
    /sbin/chkconfig --del nmb
fi
exit 0
%endif

if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable smb.service > /dev/null 2>&1 || :
    /bin/systemctl --no-reload disable nmb.service > /dev/null 2>&1 || :
    /bin/systemctl stop smb.service > /dev/null 2>&1 || :
    /bin/systemctl stop nmb.service > /dev/null 2>&1 || :
fi

%postun
%if 0%{?fedora} > 15 || 0%{?rhel} > 6
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart smb.service >/dev/null 2>&1 || :
    /bin/systemctl try-restart nmb.service >/dev/null 2>&1 || :
fi
%endif

%post -n libsmbclient4 -p /sbin/ldconfig

%postun -n libsmbclient4 -p /sbin/ldconfig

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post dc-libs -p /sbin/ldconfig

%postun dc-libs -p /sbin/ldconfig

%post test -p /sbin/ldconfig

%postun test -p /sbin/ldconfig

%pre winbind
/usr/sbin/groupadd -g 88 wbpriv >/dev/null 2>&1 || :

%post winbind
%if 0%{?rhel} == 6
/sbin/chkconfig --add winbind
if [ "$1" -ge "1" ]; then
    /sbin/service winbind condrestart >/dev/null 2>&1 || :
fi
exit 0
%endif

if [ "$1" -ge "1" ]; then
    /bin/systemctl try-restart winbind.service >/dev/null 2>&1 || :
fi

%preun winbind
%if 0%{?rhel} == 6
if [ $1 = 0 ] ; then
    /sbin/service winbind stop >/dev/null 2>&1 || :
    /sbin/chkconfig --del winbind
fi
exit 0
%endif

if [ $1 = 0 ] ; then
    /bin/systemctl stop winbind.service >/dev/null 2>&1 || :
    /bin/systemctl disable winbind.service
fi
exit 0

%post common -p /sbin/ldconfig

%postun common -p /sbin/ldconfig

%post winbind-clients -p /sbin/ldconfig

%postun winbind-clients -p /sbin/ldconfig

%post -n libwbclient -p /sbin/ldconfig

%postun -n libwbclient -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING
%{_sbindir}/nmbd
%{_sbindir}/smbd
%{_libdir}/samba/libsmbd_base.so
%{_libdir}/samba/libsmbd_conn.so
%{_libdir}/samba/auth
%{_libdir}/samba/idmap
%{_libdir}/samba/vfs
%if 0%{?fedora} > 15 || 0%{?rhel} > 6
%{_unitdir}/nmb.service
%{_unitdir}/smb.service
%endif
%config(noreplace) %{_sysconfdir}/logrotate.d/samba
%config(noreplace) %{_sysconfdir}/pam.d/samba
%attr(0700,root,root) %dir /var/log/samba
%attr(0700,root,root) %dir /var/log/samba/old
%attr(1777,root,root) %dir /var/spool/samba
%dir %{_sysconfdir}/openldap/schema
%{_sysconfdir}/openldap/schema/samba.schema
%{_sysconfdir}/tmpfiles.d/samba.conf
%if 0%{?rhel} == 6
%{_initrddir}/nmb
%{_initrddir}/smb
%endif

%files libs
%defattr(-,root,root)
%{_libdir}/libdcerpc-atsvc.so.*
%{_libdir}/libdcerpc-binding.so.*
%{_libdir}/libdcerpc-samr.so.*
%{_libdir}/libdcerpc.so.*
%{_libdir}/libgensec.so.*
%{_libdir}/libndr-krb5pac.so.*
%{_libdir}/libndr-nbt.so.*
%{_libdir}/libndr-standard.so.*
%{_libdir}/libndr.so.*
%{_libdir}/libregistry.so.*
%{_libdir}/libsamba-credentials.so.*
%{_libdir}/libsamba-hostconfig.so.*
%{_libdir}/libsamba-policy.so.*
%{_libdir}/libsamba-util.so.*
%{_libdir}/libsamdb.so.*
%{_libdir}/libsmbclient-raw.so.*
%{_libdir}/libsmbconf.so.*
%{_libdir}/libtevent-util.so.*

# libraries needed by the public libraries
%{_libdir}/samba/libCHARSET3.so
%{_libdir}/samba/libHDB_SAMBA4.so
%{_libdir}/samba/libLIBWBCLIENT_OLD.so
%{_libdir}/samba/libUTIL_TDB.so
%{_libdir}/samba/libadt_tree.so
%{_libdir}/samba/libasn1-samba4.so.8
%{_libdir}/samba/libasn1-samba4.so.8.0.0
%{_libdir}/samba/libasn1util.so
%{_libdir}/samba/libauth_sam_reply.so
%{_libdir}/samba/libauth_unix_token.so
%{_libdir}/samba/libauthkrb5.so
%{_libdir}/samba/libcli-ldap-common.so
%{_libdir}/samba/libcli-ldap.so
%{_libdir}/samba/libcli-nbt.so
%{_libdir}/samba/libcli_cldap.so
%{_libdir}/samba/libcli_smb_common.so
%{_libdir}/samba/libcliauth.so
%{_libdir}/samba/libcluster.so
%{_libdir}/samba/libdb-glue.so
%{_libdir}/samba/libdbwrap.so
%{_libdir}/samba/libdcerpc-samba.so
%{_libdir}/samba/libdcerpc-samba4.so
%{_libdir}/samba/liberrors.so
%{_libdir}/samba/libevents.so
%{_libdir}/samba/libflag_mapping.so
%{_libdir}/samba/libgse.so
%{_libdir}/samba/libgssapi-samba4.so.2
%{_libdir}/samba/libgssapi-samba4.so.2.0.0
%{_libdir}/samba/libhcrypto-samba4.so.5
%{_libdir}/samba/libhcrypto-samba4.so.5.0.1
%{_libdir}/samba/libhdb-samba4.so.11
%{_libdir}/samba/libhdb-samba4.so.11.0.2
%{_libdir}/samba/libheimbase-samba4.so.1
%{_libdir}/samba/libheimbase-samba4.so.1.0.0
%{_libdir}/samba/libhx509-samba4.so.5
%{_libdir}/samba/libhx509-samba4.so.5.0.0
%{_libdir}/samba/libinterfaces.so
%{_libdir}/samba/libkdc-policy.so
%{_libdir}/samba/libkrb5-samba4.so.26
%{_libdir}/samba/libkrb5-samba4.so.26.0.0
%{_libdir}/samba/libldbsamba.so
%{_libdir}/samba/liblibsmb.so
%{_libdir}/samba/libmemcache.so
%{_libdir}/samba/libndr-samba.so
%{_libdir}/samba/libndr-samba4.so
%{_libdir}/samba/libnetif.so
%{_libdir}/samba/libnpa_tstream.so
%{_libdir}/samba/libreplace.so
%{_libdir}/samba/libroken-samba4.so.19
%{_libdir}/samba/libroken-samba4.so.19.0.1
%{_libdir}/samba/libsamba-modules.so
%{_libdir}/samba/libsamba-net.so
%{_libdir}/samba/libsamba-sockets.so
%{_libdir}/samba/libsamba_python.so
%{_libdir}/samba/libsamdb-common.so
%{_libdir}/samba/libsecrets3.so
%{_libdir}/samba/libsecurity.so
%{_libdir}/samba/libserver-role.so
%{_libdir}/samba/libshares.so
%{_libdir}/samba/libsmbd_shim.so
%{_libdir}/samba/libsmbpasswdparser.so
%{_libdir}/samba/libsmbregistry.so
%{_libdir}/samba/libstring_init.so
%{_libdir}/samba/libtdb-wrap.so
%{_libdir}/samba/libtdb_compat.so
%{_libdir}/samba/libutil_cmdline.so
%{_libdir}/samba/libutil_malloc.so
%{_libdir}/samba/libutil_reg.so
%{_libdir}/samba/libutil_sec.so
%{_libdir}/samba/libutil_str.so
%{_libdir}/samba/libwind-samba4.so.0
%{_libdir}/samba/libwind-samba4.so.0.0.0
%{_libdir}/samba/libwrap_xattr.so

%if %{with_ldb}
%{_libdir}/samba/libldb.so.1
%{_libdir}/samba/libldb.so.%{ldb_version}
%{_libdir}/samba/libpyldb-util.so.1
%{_libdir}/samba/libpyldb-util.so.%{ldb_version}
%endif
%if %{with_talloc}
%{_libdir}/samba/libtalloc.so.2
%{_libdir}/samba/libtalloc.so.%{talloc_version}
%{_libdir}/samba/libpytalloc-util.so.2
%{_libdir}/samba/libpytalloc-util.so.%{talloc_version}
%endif
%if %{with_tevent}
%{_libdir}/samba/libtevent.so.0
%{_libdir}/samba/libtevent.so.%{tevent_version}
%endif
%if %{with_tdb}
%{_libdir}/samba/libtdb.so.1
%{_libdir}/samba/libtdb.so.%{tdb_version}
%endif

%files common
%defattr(-,root,root)
#%{_libdir}/samba/charset ???
%{_libdir}/libnetapi.so.*
%{_libdir}/samba/libsmbldap.so
%{_libdir}/samba/libgpo.so
%{_libdir}/samba/libprinting_migrate.so
%{_datadir}/samba/codepages
%attr(700,root,root) %dir /var/lib/samba/private
%config(noreplace) %{_sysconfdir}/samba/smb.conf
%config(noreplace) %{_sysconfdir}/samba/lmhosts
%config(noreplace) %{_sysconfdir}/sysconfig/samba

# common libraries
%{_libdir}/samba/libads.so
%{_libdir}/samba/libauth.so
%{_libdir}/samba/liblibcli_lsa3.so
%{_libdir}/samba/liblibcli_netlogon3.so
%{_libdir}/samba/libmsrpc3.so
%{_libdir}/samba/libnamearray.so
%{_libdir}/samba/libnet_keytab.so
%{_libdir}/samba/libpdb.so.*
%{_libdir}/samba/libpopt_samba3.so
%{_libdir}/samba/pdb

%if %with_pam_smbpass
/%{smb_lib}/security/pam_smbpass.so
%endif

%files dc
%defattr(-,root,root)
%{_bindir}/samba-tool
%{_bindir}/samba_kcc
%{_sbindir}/provision
%{_sbindir}/samba
%{_sbindir}/upgradeprovision
%{_sbindir}/samba_dnsupdate
%{_sbindir}/samba_spnupdate
%{_sbindir}/upgradedns
%{_libdir}/mit_samba.so
%{_libdir}/samba/bind9/dlz_bind9.so
%{_libdir}/samba/libcmdline-credentials.so
%{_libdir}/samba/libdfs_server_ad.so
%{_libdir}/samba/libdsdb-module.so
%{_libdir}/samba/libheimntlm-samba4.so.1
%{_libdir}/samba/libheimntlm-samba4.so.1.0.1
%{_libdir}/samba/libkdc-samba4.so.2
%{_libdir}/samba/libkdc-samba4.so.2.0.0
%{_libdir}/samba/libpac.so
%{_libdir}/samba/gensec
%{_libdir}/samba/ldb
%{_libdir}/samba/process_model
%{_libdir}/samba/service
%dir /var/lib/samba/sysvol
%{_datadir}/samba/setup
%{_mandir}/man8/samba.8.gz

%files dc-libs
%defattr(-,root,root)
%{_libdir}/libdcerpc-server.so.*
%{_libdir}/samba/libauth4.so
%{_libdir}/samba/libntvfs.so
%{_libdir}/samba/libprocess_model.so
%{_libdir}/samba/libservice.so

%files winbind
%defattr(-,root,root)
%{_bindir}/ntlm_auth3
#%{_bindir}/wbinfo3
%{_libdir}/samba/idmap
%{_libdir}/samba/nss_info
%{_libdir}/samba/libnss_info.so
%{_libdir}/samba/libidmap.so
%{_sbindir}/winbindd
%ghost %dir /var/run/winbindd
%attr(750,root,wbpriv) %dir /var/lib/samba/winbindd_privileged
%config(noreplace) %{_sysconfdir}/security/pam_winbind.conf
%if 0%{?fedora} > 15 || 0%{?rhel} > 6
%{_unitdir}/winbind.service
%endif
#%{_mandir}/man1/ntlm_auth.1*
#%{_mandir}/man1/wbinfo.1*
#%{_mandir}/man5/pam_winbind.conf.5*
#%{_mandir}/man8/pam_winbind.8*
#%{_mandir}/man8/winbindd.8*
#%{_mandir}/man8/idmap_*.8*
#%{_datadir}/locale/*/LC_MESSAGES/pam_winbind.mo
%if 0%{?rhel} == 6
%{_initrddir}/winbind
%endif

%files winbind-krb5-locator
%defattr(-,root,root)
#%{_mandir}/man7/winbind_krb5_locator.7*
%{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so

%files winbind-clients
%defattr(-,root,root)
%{_bindir}/wbinfo
%{_libdir}/libnss_winbind.so
/%{smb_lib}/libnss_winbind.so.2
%{_libdir}/libnss_wins.so
/%{smb_lib}/libnss_wins.so.2
/%{smb_lib}/security/pam_winbind.so

%files client
%defattr(-,root,root)
%{_bindir}/cifsdd
%{_bindir}/dbwrap_tool
%{_bindir}/debug2html
%{_bindir}/eventlogadm
%{_bindir}/log2pcap
%{_bindir}/net
%{_bindir}/nmblookup
%{_bindir}/nmblookup3
%{_bindir}/ntlm_auth
%{_bindir}/ntlm_auth3
%{_bindir}/oLschema2ldif
%{_bindir}/pdbedit
%{_bindir}/profiles
%{_bindir}/regdiff
%{_bindir}/regpatch
%{_bindir}/regshell
%{_bindir}/regtree
%{_bindir}/rpcclient
%{_bindir}/sharesec
%{_bindir}/smbcacls
%{_bindir}/smbclient
%{_bindir}/smbclient3
%{_bindir}/smbcontrol
%{_bindir}/smbcquotas
%{_bindir}/smbfilter
%{_bindir}/smbget
#%{_bindir}/smbiconv
%{_bindir}/smbpasswd
%{_bindir}/smbspool
%{_bindir}/smbstatus
%{_bindir}/smbta-util
%{_bindir}/smbtree
%{_bindir}/split_tokens
%{_bindir}/testparm
%{_libdir}/samba/libaddns.so
%{_libdir}/samba/libcli_spoolss.so
%{_libdir}/samba/libldb-cmdline.so
%{_libdir}/samba/libtrusts_util.so
%{_mandir}/man1/nmblookup.1.gz
%{_mandir}/man1/ntlm_auth.1.gz
%{_mandir}/man1/oLschema2ldif.1.gz
%{_mandir}/man1/regdiff.1.gz
%{_mandir}/man1/regpatch.1.gz
%{_mandir}/man1/regshell.1.gz
%{_mandir}/man1/regtree.1.gz

%if %{with_tdb}
%{_bindir}/tdbbackup
%{_bindir}/tdbdump
%{_bindir}/tdbrestore
%{_bindir}/tdbtool
%{_mandir}/man8/tdbbackup.8.gz
%{_mandir}/man8/tdbdump.8.gz
%{_mandir}/man8/tdbrestore.8.gz
%{_mandir}/man8/tdbtool.8.gz
%endif

%if %with_ldb
%{_bindir}/ldbadd
%{_bindir}/ldbdel
%{_bindir}/ldbedit
%{_bindir}/ldbmodify
%{_bindir}/ldbrename
%{_bindir}/ldbsearch
%{_mandir}/man1/ldbadd.1.gz
%{_mandir}/man1/ldbdel.1.gz
%{_mandir}/man1/ldbedit.1.gz
%{_mandir}/man1/ldbmodify.1.gz
%{_mandir}/man1/ldbrename.1.gz
%{_mandir}/man1/ldbsearch.1.gz
%endif

%files test
%defattr(-,root,root)
%{_bindir}/dbwrap_torture
%{_bindir}/gentest
%{_bindir}/locktest
%{_bindir}/locktest2
%{_bindir}/locktest3
%{_bindir}/masktest
%{_bindir}/masktest3
%{_bindir}/msgtest
%{_bindir}/ndrdump
%{_bindir}/nsstest
%{_bindir}/pdbtest
%{_bindir}/pthreadpooltest
%{_bindir}/rpc_open_tcp
%{_bindir}/smbconftort
%{_bindir}/smbtorture
%{_bindir}/smbtorture3
%{_bindir}/test_lp_load
%{_bindir}/timelimit
%{_bindir}/versiontest
%{_bindir}/vfstest
%{_bindir}/vlp
%{_libdir}/libtorture.so.*
%{_libdir}/samba/libsubunit.so
%{_mandir}/man1/gentest.1.gz
%{_mandir}/man1/locktest.1.gz
%{_mandir}/man1/masktest.1.gz
%{_mandir}/man1/ndrdump.1.gz
%{_mandir}/man1/smbtorture.1.gz

%files devel
%defattr(-,root,root)
%{_includedir}/samba-4.0/charset.h
%{_includedir}/samba-4.0/core/doserr.h
%{_includedir}/samba-4.0/core/error.h
%{_includedir}/samba-4.0/core/ntstatus.h
%{_includedir}/samba-4.0/core/werror.h
%{_includedir}/samba-4.0/credentials.h
%{_includedir}/samba-4.0/dcerpc.h
%{_includedir}/samba-4.0/dcerpc_server.h
%{_includedir}/samba-4.0/dlinklist.h
%{_includedir}/samba-4.0/domain_credentials.h
%{_includedir}/samba-4.0/gen_ndr/atsvc.h
%{_includedir}/samba-4.0/gen_ndr/auth.h
%{_includedir}/samba-4.0/gen_ndr/dcerpc.h
%{_includedir}/samba-4.0/gen_ndr/epmapper.h
%{_includedir}/samba-4.0/gen_ndr/krb5pac.h
%{_includedir}/samba-4.0/gen_ndr/lsa.h
%{_includedir}/samba-4.0/gen_ndr/mgmt.h
%{_includedir}/samba-4.0/gen_ndr/misc.h
%{_includedir}/samba-4.0/gen_ndr/nbt.h
%{_includedir}/samba-4.0/gen_ndr/drsblobs.h
%{_includedir}/samba-4.0/gen_ndr/drsuapi.h
%{_includedir}/samba-4.0/gen_ndr/ndr_drsblobs.h
%{_includedir}/samba-4.0/gen_ndr/ndr_drsuapi.h
%{_includedir}/samba-4.0/gen_ndr/ndr_atsvc.h
%{_includedir}/samba-4.0/gen_ndr/ndr_atsvc_c.h
%{_includedir}/samba-4.0/gen_ndr/ndr_dcerpc.h
%{_includedir}/samba-4.0/gen_ndr/ndr_epmapper.h
%{_includedir}/samba-4.0/gen_ndr/ndr_epmapper_c.h
%{_includedir}/samba-4.0/gen_ndr/ndr_krb5pac.h
%{_includedir}/samba-4.0/gen_ndr/ndr_mgmt.h
%{_includedir}/samba-4.0/gen_ndr/ndr_mgmt_c.h
%{_includedir}/samba-4.0/gen_ndr/ndr_misc.h
%{_includedir}/samba-4.0/gen_ndr/ndr_nbt.h
%{_includedir}/samba-4.0/gen_ndr/ndr_samr.h
%{_includedir}/samba-4.0/gen_ndr/ndr_samr_c.h
%{_includedir}/samba-4.0/gen_ndr/ndr_svcctl.h
%{_includedir}/samba-4.0/gen_ndr/ndr_svcctl_c.h
%{_includedir}/samba-4.0/gen_ndr/netlogon.h
%{_includedir}/samba-4.0/gen_ndr/samr.h
%{_includedir}/samba-4.0/gen_ndr/security.h
%{_includedir}/samba-4.0/gen_ndr/server_id.h
%{_includedir}/samba-4.0/gen_ndr/svcctl.h
%{_includedir}/samba-4.0/gensec.h
%{_includedir}/samba-4.0/ldap-util.h
%{_includedir}/samba-4.0/ldap_errors.h
%{_includedir}/samba-4.0/ldap_message.h
%{_includedir}/samba-4.0/ldap_ndr.h
%{_includedir}/samba-4.0/ldb_wrap.h
%{_includedir}/samba-4.0/lookup_sid.h
%{_includedir}/samba-4.0/machine_sid.h
%{_includedir}/samba-4.0/ndr.h
%{_includedir}/samba-4.0/ndr/ndr_drsblobs.h
%{_includedir}/samba-4.0/ndr/ndr_drsuapi.h
%{_includedir}/samba-4.0/ndr/ndr_svcctl.h
%{_includedir}/samba-4.0/ndr/ndr_nbt.h
%{_includedir}/samba-4.0/netapi.h
%{_includedir}/samba-4.0/param.h
%{_includedir}/samba-4.0/passdb.h
%{_includedir}/samba-4.0/policy.h
%{_includedir}/samba-4.0/read_smb.h
%{_includedir}/samba-4.0/registry.h
%{_includedir}/samba-4.0/roles.h
%{_includedir}/samba-4.0/rpc_common.h
%{_includedir}/samba-4.0/samba/popt.h
%{_includedir}/samba-4.0/samba/session.h
%{_includedir}/samba-4.0/samba/version.h
%{_includedir}/samba-4.0/share.h
%{_includedir}/samba-4.0/smb2_constants.h
%{_includedir}/samba-4.0/smb2_create_blob.h
%{_includedir}/samba-4.0/smb2_signing.h
%{_includedir}/samba-4.0/smb_cli.h
%{_includedir}/samba-4.0/smb_cliraw.h
%{_includedir}/samba-4.0/smb_common.h
%{_includedir}/samba-4.0/smbconf.h
%{_includedir}/samba-4.0/smb_constants.h
%{_includedir}/samba-4.0/smb_ldap.h
%{_includedir}/samba-4.0/smbldap.h
%{_includedir}/samba-4.0/smb_raw.h
%{_includedir}/samba-4.0/smb_raw_interfaces.h
%{_includedir}/samba-4.0/smb_raw_signing.h
%{_includedir}/samba-4.0/smb_raw_trans2.h
%{_includedir}/samba-4.0/smb_request.h
%{_includedir}/samba-4.0/smb_seal.h
%{_includedir}/samba-4.0/smb_signing.h
%{_includedir}/samba-4.0/smb_unix_ext.h
%{_includedir}/samba-4.0/smb_util.h
%{_includedir}/samba-4.0/tdr.h
%{_includedir}/samba-4.0/torture.h
%{_includedir}/samba-4.0/tsocket.h
%{_includedir}/samba-4.0/tsocket_internal.h
%{_includedir}/samba-4.0/samba_util.h
%{_includedir}/samba-4.0/util/attr.h
%{_includedir}/samba-4.0/util/byteorder.h
%{_includedir}/samba-4.0/util/data_blob.h
%{_includedir}/samba-4.0/util/debug.h
%{_includedir}/samba-4.0/util/memory.h
%{_includedir}/samba-4.0/util/safe_string.h
%{_includedir}/samba-4.0/util/string_wrappers.h
%{_includedir}/samba-4.0/util/talloc_stack.h
%{_includedir}/samba-4.0/util/tevent_ntstatus.h
%{_includedir}/samba-4.0/util/tevent_unix.h
%{_includedir}/samba-4.0/util/tevent_werror.h
%{_includedir}/samba-4.0/util/time.h
%{_includedir}/samba-4.0/util/xfile.h
%{_includedir}/samba-4.0/util_ldb.h
%{_libdir}/libdcerpc-atsvc.so
%{_libdir}/libdcerpc-binding.so
%{_libdir}/libdcerpc-samr.so
%{_libdir}/libdcerpc-server.so
%{_libdir}/libdcerpc.so
%{_libdir}/libgensec.so
%{_libdir}/libndr-krb5pac.so
%{_libdir}/libndr-nbt.so
%{_libdir}/libndr-standard.so
%{_libdir}/libndr.so
%{_libdir}/libnetapi.so
%{_libdir}/libregistry.so
%{_libdir}/libsamba-credentials.so
%{_libdir}/libsamba-hostconfig.so
%{_libdir}/libsamba-policy.so
%{_libdir}/libsamba-util.so
%{_libdir}/libsamdb.so
%{_libdir}/libsmbclient-raw.so
%{_libdir}/libsmbconf.so
%{_libdir}/libtevent-util.so
%{_libdir}/libtorture.so
%{_libdir}/pkgconfig/dcerpc.pc
%{_libdir}/pkgconfig/dcerpc_atsvc.pc
%{_libdir}/pkgconfig/dcerpc_samr.pc
%{_libdir}/pkgconfig/dcerpc_server.pc
%{_libdir}/pkgconfig/gensec.pc
%{_libdir}/pkgconfig/ndr.pc
%{_libdir}/pkgconfig/ndr_krb5pac.pc
%{_libdir}/pkgconfig/ndr_nbt.pc
%{_libdir}/pkgconfig/ndr_standard.pc
%{_libdir}/pkgconfig/registry.pc
%{_libdir}/pkgconfig/samba-credentials.pc
%{_libdir}/pkgconfig/samba-hostconfig.pc
%{_libdir}/pkgconfig/samba-policy.pc
%{_libdir}/pkgconfig/samba-util.pc
%{_libdir}/pkgconfig/samdb.pc
%{_libdir}/pkgconfig/smbclient-raw.pc
%{_libdir}/pkgconfig/torture.pc
%{_libdir}/samba/libpdb.so

%if %with_talloc
%{_includedir}/samba-4.0/pytalloc.h
%endif

%files python
%defattr(-,root,root,-)
%{python_sitearch}/*

%files pidl
%defattr(-,root,root,-)
%{perl_vendorlib}/Parse/Pidl*
%{_mandir}/man1/pidl*
%{_mandir}/man3/Parse::Pidl*
%attr(755,root,root) %{_bindir}/pidl

%files swat
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/xinetd.d/swat
%{_datadir}/samba/swat
%{_sbindir}/swat
#%{_mandir}/man8/swat.8*
#%attr(755,root,root) %{_libdir}/samba/*.msg

%files -n libsmbclient4
%defattr(-,root,root)
%attr(755,root,root) %{_libdir}/libsmbclient.so.*
%attr(755,root,root) %{_libdir}/libsmbsharemodes.so.*

%files -n libsmbclient4-devel
%defattr(-,root,root)
%{_includedir}/samba-4.0/libsmbclient.h
%{_includedir}/samba-4.0/smb_share_modes.h
%{_libdir}/libsmbclient.so
%{_libdir}/libsmbsharemodes.so
%{_libdir}/pkgconfig/smbclient.pc
#%{_libdir}/pkgconfig/smbsharemodes.pc
#%{_mandir}/man7/libsmbclient.7*

%files -n libwbclient
%defattr(-,root,root)
%{_libdir}/libwbclient.so.*
%{_libdir}/samba/libwinbind-client.so

%files -n libwbclient-devel
%defattr(-,root,root)
%{_includedir}/samba-4.0/wbclient.h
%{_libdir}/libwbclient.so
%{_libdir}/pkgconfig/wbclient.pc

%changelog
* Wed Aug 24 2011 Guenther Deschner <gdeschner@redhat.com> - 4.0-1
- initial package
