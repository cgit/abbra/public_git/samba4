#!/bin/bash

dir=".."
spec="samba4.spec"
samba_version="4.0.0"

if test $# -gt 0; then
    dir="$1"
fi

rm -f samba-${samba_version}.tar.bz2

git archive --remote ${dir} --prefix=samba-${samba_version}/ origin/master | bzip2 >  samba-${samba_version}.tar.bz2

md5sum samba-${samba_version}.tar.bz2 > sources

talloc_version=`grep ^VERSION ${dir}/lib/talloc/wscript | cut -d "'" -f 2`
tevent_version=`grep ^VERSION ${dir}/lib/tevent/wscript | cut -d "'" -f 2`
tdb_version=`grep ^VERSION ${dir}/lib/tdb/wscript | cut -d "'" -f 2`
ldb_version=`grep ^VERSION ${dir}/lib/ldb/wscript | cut -d "'" -f 2`

sed --in-place s/^%define\ talloc_version.*/%define\ talloc_version\ $talloc_version/ $spec
sed --in-place s/^%define\ tevent_version.*/%define\ tevent_version\ $tevent_version/ $spec
sed --in-place s/^%define\ tdb_version.*/%define\ tdb_version\ $tdb_version/ $spec
sed --in-place s/^%define\ ldb_version.*/%define\ ldb_version\ $ldb_version/ $spec
